// Declare app level module which depends on views, and components
var angular;

angular.module('adminApp', [
    'ngRoute', 
    'ngSanitize',
    'ngAnimate',
    'ngCookies',
    'adminApp.app',
    'adminApp.mainController',
    'adminApp.processController'
])
.config(function($locationProvider, $routeProvider){    
    $routeProvider.otherwise({redirectTo: '/'});
    // to remove #! from url
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });    
});
