'use strict';

var angular;

angular.module('adminApp.mainController', [])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider

    .when('/mainController', {
        templateUrl : '/components/main/main-view.html',
        controller  : 'MainCtrl'
    });
}])
.controller('MainCtrl', function($scope) {

    $scope.pageSettings = {
        logoImage: 'vizexplorer-logo.svg',
        dashboardTitle: 'Dashboard Assignment'
    };

    $scope.animateModal = false;

})
.directive('dashboardHeader', function() {
    return {
        templateUrl: '/directives/dashboard-header.html'
    };
});