'use strict';

var angular;

angular.module('adminApp.processController', ['chart.js', 'timer'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    
    .when('/processController', {
        controller: 'ProcessCtrl'
    });
}])
.controller('ProcessCtrl', function($scope, $http, $sce) {
    $scope.pageSettings = {
        addProcessButton: 'Add Process',
        saveProcessButton: 'Save Process',
        confirmDeleteProcess: 'Are you sure do you want to delete process?',
        labelProcessNumber: 'Process',
        labelCPU: 'CPU %',
        labelStatus: 'Status',
        labelInstances: 'Instances',
        labelMemory: 'Memory',
        labelUpTime: 'Up Time',
        labelRestarts: 'Restarts',
        labelAddProcessTitle: 'Add new process',
        placeHolderPercentage: 'CPU %',
        placeHolderStatus: 'Status',
        placeHolderInstances: 'Instances',
        placeHolderMemory: 'Memory',
        placeHolderEnter: 'Enter',
        formMessageError: 'Please fill all fields',
        formMessageProcessSaved: 'Process has been added!'
    };

    var refresh = function() {
        $http({
            method  : 'GET',
            url     : '/api/listallprocesses'
        })
        .then(function successCallback(data){
            //  trustAsHtml 
            $scope.$sce = $sce;
            
            $scope.listallprocesses = data; // response data 
                        
            // chart-line settings
            $scope.labels = [];
            $scope.series = [];
            $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
            $scope.options = {
                scales: {
                    yAxes: [
                        {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: false,
                        position: 'left'
                        },
                        {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: false,
                        position: 'right'
                        }
                    ]
                }
            };

        }, function errorCallback(response){
            $scope.message = response.status;
            $scope.showClass = 'alert-danger show';
        });

    };

    refresh();

    $scope.addNewProcess = function() {
        // Add to new process 
        if ($scope.formAddProcess.$valid) {
            $http({
                method: 'POST',
                url: '/api/addnewprocess',
                data: $scope.process                
            })
            .then(function successCallback(response){
                $scope.message = $scope.pageSettings.formMessageProcessSaved;
                $scope.showClass = 'alert-success show';
                refresh(); 

            }, function errorCallback(response){
                $scope.message = 'Server error:' + response.status;
                $scope.showClass = 'alert-danger show';
            });
            
        } else {
            // if the form in invalid, add response for the user            
            $scope.message = $scope.pageSettings.formMessageError;
            $scope.showClass = 'alert-danger show';
        }

    }

    $scope.removeProcess = function(id) {
        // Remove process 
        $http({
            method: 'PUT',
            url: '/api/deleteprocess',
            data: {
                id: id
            }
        })
        .then(function successCallback(){
            refresh();
            // return server response
        }, function errorCallback(response){
            refresh(); 
            $scope.message = 'Server error: ' + response.status;
        });        
    }

})
.directive('dashboardContent', function() {
    return {
        templateUrl: '/components/process/processView.html'
    };
})
.directive('modalForm', function() {
    return {
        templateUrl: '/directives/modal-form.html'
    };
})
.directive( "checkConfirmClick", [
    function() {
        return {
            priority: -1,
            restrict: 'A',
            scope: { confirmFunction: "&checkConfirmClick" },
            link: function( scope, element, attrs ){
                element.bind( 'click', function( e ){
                    // message defaults to "Are you sure?"
                    var message = attrs.checkConfirmClickMessage ? attrs.checkConfirmClickMessage : pageSettings.confirmDeleteProcess;
                    // confirm() requires jQuery
                    if(confirm(message)) {
                        scope.confirmFunction();
                    }
                });
            }
        }
    }
]);
