var angular;

angular.module('adminApp.app', [])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    // route for the main page
    .when('/', {
        templateUrl : '../components/main/mainView.html',
        controller  : 'MainCtrl'
    });
}])
.controller('MainCtrl', function($scope) {
    $scope.template = 'Home Page';
});