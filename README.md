# README #

This is the Front End App framework using AngularJS.


### Setup AngularJS Framework Dependencies ###

* Clone **das-client-side** repository inside the folder that you created, e.g. **dashboard-monitor** which we created for the Back End Project. 
For more info, visit:
[Setup Back End Project](https://bitbucket.org/schneiderstech/das-server-side#readme)
* Run **npm install**: 
This will install all modules required for the Front End  Framework AngularJS


That's everything you will need.

For more information, how to install AngularJS, please visit:
[How to install AngularJS](https://www.npmjs.com/package/angular)